from django.forms import ModelForm, TextInput, PasswordInput, EmailInput, CharField, ValidationError
from user_management.models import Client

class RegisterForm(ModelForm):
    password_confirm = CharField(widget=PasswordInput({'placeholder': 'Confirm Password'}))
    class Meta:
        model = Client
        fields = ['password', 'email']
        widgets = {
            'password': PasswordInput(attrs={'placeholder': 'Password'}),
            'email': EmailInput(attrs={'placeholder': 'Email'})
        }

    def clean_password_confirm(self):
        cleaned_data = super(RegisterForm, self).clean()
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")

        if password != password_confirm:
            raise ValidationError(
                "password and confirm_password does not match"
            )