from django.contrib import admin
from products.models import Product, Brand, Category, ProductImages, ProductAdmin

admin.site.register(Product,ProductAdmin)
admin.site.register(Brand)
admin.site.register(Category)
# admin.site.register(ProductImages)

