from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.template import RequestContext
from django.views.generic import DetailView, CreateView, ListView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from products.models import Product, ProductReview, UserProduct, Category, Brand, Notification
from products.forms import CommentForm, CartForm


class ProductsView(ListView):
    model = Product
    paginate_by = 30
    context_object_name = 'products'
    template_name = 'products/list.html'

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        context['notification'] = Notification.objects.all()
        context['categories'] = Category.objects.all()
        context['brands'] = Brand.objects.all()
        context['discounts'] = Product.objects.filter(status='Sales')
        if 'brand' in self.request.GET or 'category' in self.request.GET:
            context['search_result'] = True
        return context

    def get_queryset(self):
        if 'brand' in self.request.GET:
            return Product.objects.filter(brand_id=self.request.GET['brand'])
        elif 'category' in self.request.GET:
            return Product.objects.filter(categories__id__in=[self.request.GET['category']])
        else:
            return Product.objects.all()


class ProductDetails(DetailView):
    model = Product
    template_name = 'products/detail.html'
    context_object_name = 'product'

    def get_context_data(self, **kwargs):
        context = super(ProductDetails, self).get_context_data(**kwargs)
        context['cart_form'] = CartForm()
        return context


class CommentView(CreateView):
    model = ProductReview
    form_class = CommentForm
    template_name = 'products/comment.html'

    def get_context_data(self, **kwargs):
        context = super(CommentView, self).get_context_data(**kwargs)
        context['comments'] = ProductReview.objects.filter(product__pk=self.kwargs['product_pk'])
        context['product'] = Product.objects.get(pk=self.kwargs['product_pk'])
        context['user'] = self.request.user
        return context

    def form_invalid(self, form):
        return redirect('product_detail', pk=self.kwargs['product_pk'])

    def form_valid(self, form):
        if not self.request.user or not self.request.user.is_authenticated:
            return redirect('account_login')

        comment = ProductReview()
        comment.content = form.cleaned_data['content']
        comment.stars = form.cleaned_data['stars']
        comment.product_id = self.kwargs['product_pk']
        comment.user_id = self.request.user.id
        comment.save()
        return redirect('product_detail', pk=self.kwargs['product_pk'])


@login_required(login_url='account_login')
def add_to_cart(request, *args, **kwargs):
    if request.method == 'POST':
        form = CartForm(request.POST)
        if form.is_valid():
            cart_queryset = UserProduct.objects.filter(product_id=kwargs['product_pk'], user_id=request.user.id, type=0)
            if cart_queryset.__len__() >= 1:
                cart = cart_queryset.first()
                cart.quantity = cart.quantity + form.cleaned_data['quantity']
                cart.save()
            else:
                cart = UserProduct()
                cart.quantity = form.cleaned_data['quantity']
                cart.product_id = kwargs['product_pk']
                cart.user_id = request.user.id
                cart.type = 0
                cart.save()

            context = RequestContext(request)
            context['add_success'] = True
            context['cart_form'] = CartForm()
            return JsonResponse({'status': 200})


def search_products(request, *args, **kwargs):
    if request.method == 'POST':
        context = {}
        context['categories'] = Category.objects.all()
        context['brands'] = Brand.objects.all()
        context['search_result'] = True
        context['products'] = Product.objects.filter(name__icontains=request.POST['search_params'])[:10]
        return render(request, 'products/list.html', context=context, status=200)
