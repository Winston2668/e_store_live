from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView
from e_store import settings
from order.models import Order, OrderItem
from products.models import UserProduct
from cart.forms import PostInfoForm

@method_decorator(login_required, name='get')
class CartView(ListView):
    template_name = 'cart/cart.html'
    model = UserProduct
    context_object_name = 'user_items'


    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        if self.request.user.post_addresses.exists():
            context['form'] = PostInfoForm(instance=self.request.user.post_addresses.first())
        else:
            context['form'] = PostInfoForm()

        total = 0
        order_items = []
        for record in UserProduct.objects.filter(user_id=self.request.user.id):
            item = OrderItem()
            item.product = record.product
            item.quantity = record.quantity
            total = total + record.product.price * record.quantity
            order_items.append(item)
        context['total'] = total

        if order_items.__len__() > 0:
            order = Order()
            packages = order.pack_items(order_items)
            fee = 0
            for package in packages:
                # Unit transform.
                package['weight'] = package['weight']/1000.0
                fee += settings.UNIT_POST_PRICE if package['weight'] + settings.PACKAGE_WEIGHT <= 1 else settings.UNIT_POST_PRICE * (package['weight'] +  + settings.PACKAGE_WEIGHT)
            # Currency transform.
            context['post'] = round(fee * settings.CURRENCY_RATE, 2)
        else:
            context['post'] = 0

        context['all'] = context['post'] + context['total']

        return context

    def get_queryset(self):
        return UserProduct.objects.filter(user_id=self.request.user.id)

@login_required(login_url='account_login')
def delete_item(request, *args, **kwargs):
    cart_item = UserProduct.objects.get(pk=request.POST['item_id'])
    cart_item.delete()
    return JsonResponse({}, status=200)

