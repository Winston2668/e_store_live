"""e_store URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

from e_store import settings
from django.conf.urls.static import static

urlpatterns = [
                  url(r'^$', include('products.urls')),
                  url(r'^ckeditor/', include('ckeditor_uploader.urls')),
                  url(r'^admin_sunny/', admin.site.urls),
                  url(r'^user/', include('user_management.urls')),
                  url(r'^accounts/', include('allauth.urls')),
                  url(r'^products/', include('products.urls')),
                  url(r'^cart/', include('cart.urls')),
                  url(r'^order/', include('order.urls')),
                  url(r'^contact-us/', TemplateView.as_view(template_name='global/contact.html'), name='contact_us'),
                  url(r'^tutorial/', TemplateView.as_view(template_name='global/tutorial.html'), name='tutorial'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# ... your normal urlpatterns here