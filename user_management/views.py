# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# from django.shortcuts import render
# from django.views.generic.edit import FormView, CreateView
# from django.urls import reverse
# from django.contrib.auth.models import User
# from post_office import mail
# from e_store import settings
# from user_management.forms import RegisterForm
# from user_management.models import Client
#
# class RegisterView(CreateView):
#     form_class = RegisterForm
#     template_name = 'user_management/register.html'
#
#     def form_valid(self, form):
#         user = Client.objects.create_user(
#                 username=form.cleaned_data['email'],
#                 password=form.cleaned_data['password'],
#                 email=form.cleaned_data['email']
#         )
#         user.save()
#         mail.send(
#             recipients = [form.cleaned_data['email']],
#             sender = form.cleaned_data['email'],
#             subject='您好，您已成功注册！',
#             message=reverse('verify_user', kwargs={'verify_code': user.registration_code}),
#             priority='now'
#         )
#
# def verify_user(request, *args, **kwargs):
#     pass
#
