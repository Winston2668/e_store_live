from django.contrib import admin
from order.models import Order, OrderAdmin, SubittedOrder, SubittedOrderAdmin, PaidOrder, PaidOrderAdmin

admin.site.register(Order, OrderAdmin)
admin.site.register(SubittedOrder, SubittedOrderAdmin)
admin.site.register(PaidOrder, PaidOrderAdmin)