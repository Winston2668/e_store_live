import datetime
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator, MaxLengthValidator

from e_store import settings
from django.db import models
from django.contrib.auth.models import User
from products.models import Product

class Order(models.Model):
    ORDER_STATUS = (
        ('Submitted', '已提交'),
        ('Charged', '已付款'),
        ('Delivered', '已发货'),
        ('Done', '完成'),
    )
    order_number = models.CharField(max_length=64, null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=datetime.datetime.now())
    status = models.CharField(max_length=16, default='Submitted', choices=ORDER_STATUS)
    cost = models.FloatField(null=True, blank=True)
    post = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name = '所有订单'
        verbose_name_plural = '所有订单'

    def __str__(self):
        if self.user.post_addresses.all():
            return self.user.post_addresses.all()[0].receiver_name + ' ' + self.created_date.strftime("%Y-%m-%d")
        else:
            return self.order_number

    @property
    def status_verbose(self):
        return dict(Order.ORDER_STATUS)[self.status]


    @property
    def post_fee(self):
        packages = self.pack_items(self.order_items.all())
        fee = 0
        for package in packages:
            # Unit transform.
            package['weight'] = package['weight']/1000.0
            fee += settings.UNIT_POST_PRICE if package['weight'] + settings.PACKAGE_WEIGHT <= 1 else settings.UNIT_POST_PRICE * (package['weight'] +  + settings.PACKAGE_WEIGHT)
        # Currency transform.
        return round(fee * settings.CURRENCY_RATE, 2)

    @property
    def total(self):
        return self.post + self.cost

    def pack_items(self, items):
        products = []
        for order_item in items:
            if not order_item.product.post_fee_included:
                for i in range(0, order_item.quantity):
                    if not order_item.product.post_fee_included:
                        products.append(order_item.product)
        packages = []
        products.sort(key=lambda x: x.unit, reverse=True)
        if products.__len__() > 0:
            while True:
                unpacked = []
                package = {'units': 0, 'items': [], 'weight': 0, 'limit_num': 12/products[0].unit}
                item_num = 0
                for product in products:
                    if package['units'] + product.unit <= 12 and item_num + 1 <= package['limit_num']:
                        package['units'] = package['units'] + product.unit
                        package['items'].append(product)
                        package['weight'] = package['weight'] + product.weight
                        item_num += 1
                    else:
                        unpacked.append(product)
                result = (package, unpacked)

                packages.append(result[0])
                if result[1].__len__() > 0:
                    products = result[1]
                    products.sort(key=lambda x: x.unit, reverse=True)
                else:
                    break
            return packages
        else:
            return []


class OrderItem(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True)
    quantity = models.IntegerField()
    order = models.ForeignKey(Order, related_name='order_items', null=True, blank=True, on_delete=models.CASCADE)

def validate_phone(value):
    if str(value).__len__() != 11 or not str(value).isdigit():
        raise ValidationError(
            '请输入一个合法的手机号码'
        )

class PostInfo(models.Model):
    address = models.CharField(max_length=128)
    receiver_name = models.CharField(max_length=16)
    phone = models.CharField(max_length=16, validators=[validate_phone])
    zip = models.CharField(max_length=16, blank=True, null=True)
    user = models.ForeignKey(User, related_name='post_addresses')



class OrderItemInline(admin.TabularInline):
    model = OrderItem


class OrderAdmin(admin.ModelAdmin):
    inlines = [
        OrderItemInline,
    ]


class SubittedOrder(Order):
    class Meta:
        proxy = True
        verbose_name = '提交的未付款订单'
        verbose_name_plural = '提交的未付款订单'

class SubittedOrderAdmin(OrderAdmin):
    def get_queryset(self, request):
        return  self.model.objects.filter(status='Submitted')



class PaidOrder(Order):
    class Meta:
        proxy = True
        verbose_name = '付款订单'
        verbose_name_plural = '付款订单'


class PaidOrderAdmin(OrderAdmin):
    def get_queryset(self, request):
        return self.model.objects.filter(status='Charged')
