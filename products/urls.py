"""e_store URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from products.views import ProductDetails, CommentView, add_to_cart, ProductsView, search_products

urlpatterns = [
   url(r'^$', ProductsView.as_view(), name='products_list'),
   url(r'^detail/(?P<pk>\d+)/$', ProductDetails.as_view(), name='product_detail'),
   url(r'^detail/(?P<product_pk>\d+)/comments/$', CommentView.as_view(), name='product_comments'),
   url(r'^detail/(?P<product_pk>\d+)/add_to_cart/$', add_to_cart, name='add_to_cart'),
   url(r'^search-products/$', search_products, name='search_products'),
]
