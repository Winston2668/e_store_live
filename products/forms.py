from django import forms
from django.forms import TextInput, Textarea
from products.models import ProductReview, UserProduct

class CommentForm(forms.ModelForm):

    class Meta:
        model = ProductReview
        widgets = {
             'stars': forms.HiddenInput()
        }
        fields = ['content', 'stars']


class CartForm (forms.ModelForm):
    class Meta:
        model = UserProduct
        widgets = {
            'quantity': forms.TextInput(attrs={'min': 1, 'step':1})
        }
        fields = ['quantity']