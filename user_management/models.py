from __future__ import unicode_literals

import uuid
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


# class Client(User):
#     verified = models.BooleanField(default=False)
#     registration_code = models.UUIDField(default=uuid.uuid4)
