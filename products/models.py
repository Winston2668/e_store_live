from __future__ import unicode_literals

from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.utils import timezone
from django.contrib import admin
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField


class Category(models.Model):
    name = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        verbose_name = '产品类别'
        verbose_name_plural = '产品类别'

    def __str__(self):
        return self.name


class Notification(models.Model):
    description = models.TextField()

class Brand(models.Model):
    name = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        verbose_name = '产品品牌'
        verbose_name_plural = '产品品牌'

    def __str__(self):
        return self.name


class Product(models.Model):
    PRODUCT_STATUS = (
        ('New', '新品'),
        ('Sales', '折扣'),
    )
    name = models.CharField(max_length=128)
    description = RichTextUploadingField(null=True, blank=True)
    price = models.FloatField()
    brand = models.ForeignKey(Brand, null=True, blank=True)
    stars = models.IntegerField(default=5, null=True, blank=True)
    categories = models.ManyToManyField(Category, null=True, blank=True)
    status = models.CharField(choices=PRODUCT_STATUS, null=True, blank=True, max_length=64)
    weight = models.FloatField(null=True, blank=True)
    unit = models.IntegerField(null=True, blank=True)
    post_fee_included = models.BooleanField(default=False)

    class Meta:
        verbose_name = '我的产品'
        verbose_name_plural = '我的产品'


    def __str__(self):
        return self.name


class ProductReview(models.Model):
    content = models.TextField()
    stars = models.FloatField(null=True, blank=True, default=5)
    user = models.ForeignKey(User, blank=True, null=True)
    product = models.ForeignKey('Product', blank=True, null=True)
    created_time = models.DateTimeField(default=timezone.now())

    class Meta:
        verbose_name = '产品评价'
        verbose_name_plural = '产品评价'


class ProductImages(models.Model):
    product = models.ForeignKey('Product', related_name='images')
    image = models.ImageField()

    class Meta:
        verbose_name = '产品图片'
        verbose_name_plural = '产品图片'


    def __str__(self):
        return self.product.name


class ProductImageInline(admin.TabularInline):
    model = ProductImages


class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ProductImageInline,
    ]


class UserProduct(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    product = models.ForeignKey(Product, null=True, blank=True)
    quantity = models.IntegerField(default=0)
    created_time = models.DateTimeField(null=True, blank=True, default=timezone.now())
    type = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = '购物车管理'
        verbose_name_plural = '购物车管理'
    # 0. in cart
    # 1. submitted
    # 2. processed
    # 3. whishlist
