import datetime
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView
from django.views.generic import ListView
from django.shortcuts import render, redirect

from cart.forms import PostInfoForm
from order.models import Order, OrderItem
from products.models import UserProduct

@method_decorator(login_required, name='get')
class OrdersView(ListView):
    model = Order
    context_object_name = 'orders'
    template_name = 'order/list.html'

    def get_queryset(self):
        return Order.objects.filter(user_id=self.request.user.id)

@method_decorator(login_required, name='post')
class OrderView(CreateView):
    model = Order
    context_object_name = 'order'

@login_required(login_url='account_login')
@transaction.atomic
def generate_order(request, *args, **kwargs):
    form = PostInfoForm(request.POST)
    if form.is_valid():
        # Wrap the form data:
        item_list = []
        for key, value in request.POST.items():
            if key.isdigit():
                item = UserProduct.objects.get(pk=int(key))
                item.quantity = value
                item_list.append(item)

        order = Order()
        order.save()
        order.order_number = request.user.username.upper() + ''.join(c for c in datetime.datetime.now().__str__() if c.isdigit())
        order.cost = 0
        order.post = '待定'
        order.user_id = request.user.id
        for item in item_list:
            order_item = OrderItem()
            order_item.quantity = item.quantity
            order_item.product = item.product
            order_item.order = order
            order_item.save()
            order.cost += round(float(item.product.price) * float(item.quantity), 2)
        order.post = order.post_fee
        order.save()

        # Remove cart items.
        for item in item_list:
            item.delete()

        if not request.user.post_addresses.exists():
            post_info = form.instance
            post_info.user = request.user
            post_info.save()
        else:
            post_info = form.instance
            old_post_info = request.user.post_addresses.first()
            old_post_info.receiver_name = post_info.receiver_name
            old_post_info.address = post_info.address
            old_post_info.phone = post_info.phone
           # old_post_info.zip = post_info.address
            old_post_info.save()

        return redirect('order_list')
    else:
        items = UserProduct.objects.filter(user_id=request.user.id)
        return render(request, 'cart/cart.html', {'form': form, 'user_items': items})