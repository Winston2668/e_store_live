from django import forms
from django.forms import TextInput, Textarea
from order.models import PostInfo

class PostInfoForm(forms.ModelForm):

    class Meta:
        model = PostInfo
        fields = ['address', 'receiver_name', 'phone',]
        widgets = {
            'address': Textarea(attrs={'class': 'form-control'}),
            'receiver_name': TextInput(attrs={'class': 'form-control'}),
            'phone': TextInput(attrs={'class': 'form-control'}),
          #  'zip': TextInput(attrs={'class': 'form-control'}),
        }